/*
* Copyright (c) 1993-1997, Silicon Graphics, Inc.
* ALL RIGHTS RESERVED
* Permission to use, copy, modify, and distribute this software for
* any purpose and without fee is hereby granted, provided that the above
* copyright notice appear in all copies and that both the copyright notice
* and this permission notice appear in supporting documentation, and that
* the name of Silicon Graphics, Inc. not be used in advertising
* or publicity pertaining to distribution of the software without specific,
* written prior permission.
*
* THE MATERIAL EMBODIED ON THIS SOFTWARE IS PROVIDED TO YOU "AS-IS"
* AND WITHOUT WARRANTY OF ANY KIND, EXPRESS, IMPLIED OR OTHERWISE,
* INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY OR
* FITNESS FOR A PARTICULAR PURPOSE.  IN NO EVENT SHALL SILICON
* GRAPHICS, INC.  BE LIABLE TO YOU OR ANYONE ELSE FOR ANY DIRECT,
* SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY
* KIND, OR ANY DAMAGES WHATSOEVER, INCLUDING WITHOUT LIMITATION,
* LOSS OF PROFIT, LOSS OF USE, SAVINGS OR REVENUE, OR THE CLAIMS OF
* THIRD PARTIES, WHETHER OR NOT SILICON GRAPHICS, INC.  HAS BEEN
* ADVISED OF THE POSSIBILITY OF SUCH LOSS, HOWEVER CAUSED AND ON
* ANY THEORY OF LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE
* POSSESSION, USE OR PERFORMANCE OF THIS SOFTWARE.
*
* US Government Users Restricted Rights
* Use, duplication, or disclosure by the Government is subject to
* restrictions set forth in FAR 52.227.19(c)(2) or subparagraph
* (c)(1)(ii) of the Rights in Technical Data and Computer Software
* clause at DFARS 252.227-7013 and/or in similar or successor
* clauses in the FAR or the DOD or NASA FAR Supplement.
* Unpublished-- rights reserved under the copyright laws of the
* United States.  Contractor/manufacturer is Silicon Graphics,
* Inc., 2011 N.  Shoreline Blvd., Mountain View, CA 94039-7311.
*
* OpenGL(R) is a registered trademark of Silicon Graphics, Inc.
*/

/*
* hello.c
* This is a simple, introductory OpenGL program.
*/


#include <cmath>
#include "struct.h"
#include <GL/glut.h>
#include "hermit.h"

hermit herm;

void drawTriangle(float size ,const float posX, const float posY, float r, float g, float b)
{
	glBegin(GL_LINES);
	glLineWidth(2.5);
	glColor3f(r, g, b);
	glVertex3f(posX - size/2, posY - size/2, 0.0);
	glVertex3f(posX + size/2, posY + size/2, 0.0);
	glEnd();

	glBegin(GL_LINES);
	glLineWidth(2.5);
	glColor3f(r, g, b);

	glVertex3f(posX + size/2, posY + size/2, 0.0);
	glVertex3f(posX + size/2, posY - size/2, 0.0);
	glEnd();

	glBegin(GL_LINES);
	glLineWidth(2.5);
	glColor3f(r, g, b);


	glVertex3f(posX + size/2, posY - size/2, 0.0);
	glVertex3f(posX - size/2, posY - size/2, 0.0);
	glEnd();

}

void drawCircle(float rayon, float precision, float posX,float posY)
{
	float theta = (2*3.14)/precision;
	float thetaCurrent = 0;

	for (int i = 0; i<precision; i++)
	{
		glBegin(GL_LINES);
		glLineWidth(2.5);
		glColor3f(1, 1, 1);

		glVertex3f(rayon * cos( thetaCurrent ) + posX, rayon * sin ( thetaCurrent ) + posY, 0.0);
		glVertex3f(rayon * cos( thetaCurrent+theta )+ posX, rayon * sin ( thetaCurrent+theta ) + posY, 0.0);
		glEnd();

		thetaCurrent += theta;
	}

}

void drawCircleInside(float rayon, float precisionCercle, float iteration, float posX,float posY, float decalageX)
{
	float rayonCurrent = rayon;

	float posXCurrent = posX;
	float posYCurrent = posY;



	for (int i =0; i< iteration ; i++)
	{
		drawCircle(rayonCurrent,precisionCercle,posXCurrent,posYCurrent);

		rayonCurrent -= decalageX/iteration;
		posXCurrent -= decalageX/iteration;

		//drawCircle(0.1,100,0.4,0.5);
	}

}

void drawCircleInline(float rayon, float precisionCercle, float iteration, float facteurDeCroissance, float posX,float posY)
{
	float rayonCurrent = rayon;
	float posXCurrent = posX;
	for (int i =0; i< iteration ; i++)
	{
		drawCircle(rayonCurrent,precisionCercle,posXCurrent,posY);
		posXCurrent += rayonCurrent;
		rayonCurrent *= facteurDeCroissance;
		posXCurrent += rayonCurrent;
	}
}

void drawLine(point3 p1,point3 p2, point3 color)
{
	glBegin(GL_LINES);
	glLineWidth(2.5);
	glColor3f(color.x, color.y, color.z);

	glVertex3f(p1.x,p1.y,p1.z);
	glVertex3f(p2.x,p2.y,p2.z);
	glEnd();
}




void drawKoch(point3 a, point3 b, int iteration)
{

	point3 ab = b-a;

	point3 i = a + ab*(1/2.0);
	
	point3 m = a + (ab)*(1/3.0);
	point3 n = b - (ab)*(1/3.0);
	point3 am = m - a;
	float temp = am.x;
	am.x = am.y;
	am.y = -temp;
	point3 o = i + am*sin(-3.14/3.0);

	if (iteration > 1)
	{
		drawKoch(a,m,iteration-1);
		drawKoch(m,o,iteration-1);
		drawKoch(o,n,iteration-1);
		drawKoch(n,b,iteration-1);
	}
	else
	{
		drawLine(a,m,point3(1,1,1));
		drawLine(m,o,point3(0,1,1));
		drawLine(o,n,point3(1,0,1));
		drawLine(n,b,point3(1,1,0));
	}
}




void display(void)
{
	/* clear all pixels  */
	glClear (GL_COLOR_BUFFER_BIT);

	/* draw white polygon (rectangle) with corners at
	* (0.25, 0.25, 0.0) and (0.75, 0.75, 0.0) 
	*/

	/*glBegin(GL_POLYGON);
	glColor3f (1.0, 1.0, 1.0);
	glVertex3f (0.25, 0.50, 0.0);
	glVertex3f (0.75, 0.25, 0.0);
	glVertex3f (0.75, 0.75, 0.0);
	glVertex3f (0.25, 0.75, 0.0);
	glEnd();*/


	/* EX 1
	drawTriangle(0.3,0.5,0.5,1,1,1);
	drawTriangle(0.1,0.3,0.5,1,0.3,0.5);
	drawTriangle(0.4,0.1,0.5,0.5,0.2,0.1);*/

	// EX 2
	//drawCircleInside(0.5,100,10,0.5,0.5,0.2);


	// EX 3
	//drawKoch(point3(0.1,0.3,0),point3(0.5,0.9,0),1);
	//drawKoch(point3(0.5,0.9,0),point3(0.9,0.3,0),2);
	//drawKoch(point3(0.9,0.3,0),point3(0.1,0.3,0),3);


	// Hermit

	point3 a = herm.pointsToDisplay[0];
	point3 b;

	for (int i = 1; i < herm.pointsToDisplay.size()  ; i ++)
	{
		//printf("%f,%f,%f,%d - ", herm.pointsToDisplay[i].x,herm.pointsToDisplay[i].y,herm.pointsToDisplay[i].z,i);
		b = herm.pointsToDisplay[i];
		drawLine(a,b,point3(1,0,0));
		a = b;
	}


	for (int i = 0; i < herm.pointsControls.size()  ; i ++)
	{
		drawCircle(0.01f,100,herm.pointsControls[i].x,herm.pointsControls[i].y);
	}




	/* Swap the buffers to show the one
	* on which we writed
	*/
	glutSwapBuffers();
}


void init (void)
{
	/* select clearing color     */
	glClearColor (0.0, 0.0, 0.0, 0.0);

	/* initialize viewing values  */
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
}



/*
* Declare initial window size, position, and display mode
* (double buffer and RGB).  Open window with "hello"
* in its title bar.  Call initialization routines.
* Register callback function to display graphics.
* Enter main loop and process events.
*/
int main(int argc, char** argv)
{
/*	herm.load( 
		hermit::HermitPoint(point3(0,0,0),point3(1,0,0)),
		hermit::HermitPoint(point3(1,1,0),point3(1,0,0))
		);*/


	// Hermit Points
	std::vector<hermit::HermitPoint> controlPoints;
	controlPoints.push_back(hermit::HermitPoint(point3(0.1,0.1,0),point3(1,0,0)));
	controlPoints.push_back(hermit::HermitPoint(point3(0.3,0.8,0),point3(0,1,0)));
	controlPoints.push_back(hermit::HermitPoint(point3(0.3,0.6,0),point3(1,-1,0)));
	controlPoints.push_back(hermit::HermitPoint(point3(0.7,0.6,0),point3(1,-1,0)));
	controlPoints.push_back(hermit::HermitPoint(point3(1,0.6,0),point3(1,-1,0)));
	

	herm.load(controlPoints);


	glutInit(&argc, argv);
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize (500, 500);
	glutInitWindowPosition (100, 100);
	glutCreateWindow ("hello");
	init ();

	

	glutDisplayFunc(display);
	glutMainLoop();
	return 0;   /* ANSI C requires main to return int. */
}

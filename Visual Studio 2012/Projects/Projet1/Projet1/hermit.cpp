#include "hermit.h"


hermit::hermit(void)
{
}


hermit::~hermit(void)
{
}

void hermit::load(std::vector<HermitPoint> hermitPoints, double resolution)
{
	HermitPoint a = hermitPoints[0];
	pointsControls.push_back(hermitPoints[0].position);
	HermitPoint b;
	for (int i = 1; i < hermitPoints.size()  ; i ++)
	{
		b = hermitPoints[i];
		load(a,b,resolution);
		a = b;

		pointsControls.push_back(hermitPoints[i].position);
	}

	
	
}

void hermit::load(HermitPoint hermitPointA, HermitPoint hermitPointB, double resolution)
{
	

	for (double i = 0; i < resolution ; i ++)
	{
		double u = i/resolution;
		//printf("i: %f, res: %f, u: %f --",i, resolution, u );

		double F1 = 2*std::pow( u, 3) - 3 * std::pow(u,2) + 1;

		double F2 = -2*std::pow( u, 3) + 3 * std::pow(u,2) ;

		double F3 = std::pow( u, 3) - 2 * std::pow(u,2) + u;

		double F4 = std::pow( u, 3) - std::pow(u,2);


		pointsToDisplay.push_back
			(
				hermitPointA.position * F1 + hermitPointB.position * F2 + hermitPointA.direction  * F3 + hermitPointB.direction * F4
			);
		
	}



}

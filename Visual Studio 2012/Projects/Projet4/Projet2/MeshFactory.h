#pragma once

#include "Vector3D.h"
#include "Mesh.h"
#include "Bezier.h"

class MeshFactory
{
	// All generated object are centred

public:
	MeshFactory(void);
	~MeshFactory(void);

	static Mesh cube(Vector3D size);

	static Mesh circle(float rayon, float precision);

	static Mesh cylindre(Vector3D size, float precision);

	static Mesh sphere(float rayon, float precision);

	static Mesh drawCloath(Bezier a, Bezier b);

	static Mesh drawSurfaceBalayee(const Bezier rail, const Mesh patron);

	static Mesh plane(Vector3D A, Vector3D B, Vector3D C, Vector3D D);

	// Norm reverse clockwise
	//static Mesh triangle(Vector3D a, Vector3D b, Vector3D c);
};


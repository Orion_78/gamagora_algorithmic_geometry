#pragma once

#define _USE_MATH_DEFINES

#include <math.h>
#include <iostream>
#include <fstream>


class Tools
{
public:
	Tools(void);
	~Tools(void);


	static double rad(double v)
	{
		return v / 2 * M_PI;
	}

	static std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
		std::stringstream ss(s);
		std::string item;
		elems.clear();
		while (std::getline(ss, item, delim)) {
			elems.push_back(item);
		}
		return elems;
	}
};




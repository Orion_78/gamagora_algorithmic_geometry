#pragma once

#include "opengl.h"
#include "Scene.h"


class BezierControl
{
public:
	BezierControl();
	~BezierControl(void);

	std::vector<Bezier> * controledBezier;

	int currentBezierSelected;
	int currentPointSelected;

	
	//! Quand une touche est press�e
	GLvoid keyPressed(unsigned char key, int x, int y);

	void draw();

};


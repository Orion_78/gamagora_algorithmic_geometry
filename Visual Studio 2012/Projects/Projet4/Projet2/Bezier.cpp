#include "Bezier.h"
#include "MeshFactory.h"


Bezier::Bezier(void)
{
}


Bezier::~Bezier(void)
{
	
}


// TODO Redefinir la maniere de contruire le bezier pour avoir plus de 4 points 
// 3 point -> 4eme points par la tangeant des deux d'avant
Bezier::Bezier(const std::vector<Vector3D> controlPoints, int precision) 
{
/*	std::vector<Vector3D> mineBezier;

	
	if (controlPoints.size() > 4)
	{
		std::vector<Vector3D> otherBezier;

		Vector3D dir = controlPoints[3] - controlPoints[2];
		Vector3D liaison = controlPoints[3] + dir;

		otherBezier.push_back(controlPoints[3]);
		otherBezier.push_back(liaison);
		
		for (int i = 3; i < controlPoints.size(); i++)
		{
			otherBezier.push_back(controlPoints[i]);
		}

		for (int i = 0; i < 4; i++)
		{
			mineBezier.push_back(controlPoints[i]);
		}

		decoupeBezier = new Bezier(otherBezier,precision);
	}
	else
	{
		mineBezier = controlPoints;
	}

	currentPointSelected = -1;
	this->controlPoints = mineBezier;
	this->precision = precision;*/

	//currentPointSelected = -1;
	this->controlPoints = controlPoints;
	this->precision = precision;

	construct();
}

std::vector<Vector3D> Bezier::construct(std::vector<Vector3D> cP)
{
	for ( float t = 0; t < 1 ; t += 1.f / precision )
	{
		curvePoints.push_back(Bernstein(cP,t));
	}

	curvePoints.push_back(Bernstein(cP,1));

	//curvePoints.push_back(controlPoints[controlPoints.size()-1]);

	// INUTILE
	return curvePoints;
}

void Bezier::construct()
{
	curvePoints.clear();

	std::vector<Vector3D> dynamicControlPoints = controlPoints;

	std::vector<Vector3D> points;
	for (int i = 0; i < dynamicControlPoints.size() ; i++)
	{
		if (points.size() == 4)
		{
			construct(points);
			
			
			Vector3D dir = points[3] - points[2];
			Vector3D liaison = points[3] + dir;

			dynamicControlPoints.insert(dynamicControlPoints.begin()+i,liaison);

			i--;
			points.clear();
		}

		//dynamicControlPoints.push_back(controlPoints[i]);
		points.push_back(dynamicControlPoints[i]);

		

			/*
		std::vector<Vector3D> otherBezier;

		Vector3D dir = controlPoints[i+3] - controlPoints[i+2];
		Vector3D liaison = controlPoints[i+3] + dir;

		otherBezier.push_back(controlPoints[i+3]);
		otherBezier.push_back(liaison);
		otherBezier.push_back(controlPoints[i]);
		

		for (int i = 0; i < 4; i++)
		{
			mineBezier.push_back(controlPoints[i]);
		}

		decoupeBezier = new Bezier(otherBezier,precision);*/
	}


	if (points.size() != 0)
	{
		construct(points);
		//points.clear();
	}

	/*if (decoupeBezier != 0)
	{
		std::vector<Vector3D> a = decoupeBezier->construct();

		for (int i = 0; i < a.size(); i++)
		{
			curvePoints.push_back(a[i]);
		}
	}*/

	//return curvePoints;
}



Vector3D Bezier::Bernstein(std::vector<Vector3D> cP, float t)
{
	if (cP.size() == 4)
	{
		return cP[0]*std::pow(1-t,3) + 3 * cP[1]*t*std::pow(1-t,2) + 3 * cP[2] * pow(t,2) * (1 - t) + cP[3] * std::pow(t,3);
	}
	else if (cP.size() == 3)
	{
		return cP[0]*std::pow(1-t,2) + 2 * cP[1] * t * (1-t) + cP[2] * pow(t,2);
	}
	else if (cP.size() == 2)
	{
		return cP[0] * t + cP[1] * (1-t);
	}
	else
	{
		return Vector3D();
	}
}

/*Mesh Bezier::construct()
{
	/*Mesh m ;
	for (int i = 1; i < controlPoints.size(); i++)
	{
		Mesh temp = MeshFactory::cylindre(Vector3D(1,(controlPoints[i-1] - controlPoints[i]).magnitude(),1), 10);
		temp.translate(controlPoints[i-1] - ( controlPoints[i-1] - controlPoints[i] )/2);
	//	m.rotation
		m = m + temp;
	}*/

/*	Mesh m;
	m.m_tabPoint = controlPoints;
	for (int i = 1; i < controlPoints.size() ; i++)
	{
		m.m_tabTopologie.push_back(i-1);
		m.m_tabTopologie.push_back(i);
		m.m_tabTopologie.push_back(i);
	}

	m.computeNormal();*

	return m;


	
}*/

void Bezier::draw()
{
	Vector3D previous = controlPoints[0];
	Vector3D next;
	glColor3f(0.7,0.7,0.7);
	glLineWidth(2.5);
		
	glBegin(GL_LINES);
	for (int i =1; i < curvePoints.size();i++)
	{
		glVertex3f(curvePoints[i-1].x,curvePoints[i-1].y,curvePoints[i-1].z);
		glVertex3f(curvePoints[i].x,curvePoints[i].y,curvePoints[i].z);
	}
	glEnd();	
}
#pragma once

#include "Vector3D.h"

class Matrice4x4
{
public:
	Matrice4x4(void) : x1(0), x2(0), x3(0), y1(0), y2(0), y3(0), z1(0), z2(0), z3(0) 
	{
	}

	~Matrice4x4(void)
	{
	}

	Matrice4x4(double x1, double x2, double x3, double y1, double y2, double y3, double z1, double z2, double z3 ) : x1(x1), x2(x2), x3(x3), y1(y1), y2(y2), y3(y3), z1(z1), z2(z2), z3(z3) 
	{
	}

	double x1,x2,x3,y1,y2,y3,z1,z2,z3;

	Vector3D multiply(const Vector3D& v) const
	{
		return Vector3D(
		x1*v.x + x2*v.y + x3*v.z,
		y1*v.x + y2*v.y + y3*v.z,
		z1*v.x + z2*v.y + z3*v.z);

		
	}

	Matrice4x4 multiply(const Matrice4x4& m) const
	{
		return Matrice4x4(
			x1*m.x1, x2*m.x2, x3*m.x3,
			y1*m.y1, y2*m.y2, y3*m.y3,
			z1*m.z1, z2*m.z2, z3*m.z3);
	}
	
};




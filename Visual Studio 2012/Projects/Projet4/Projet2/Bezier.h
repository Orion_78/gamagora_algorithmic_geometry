#pragma once

#include "Vector3D.h"
#include <vector>



class Bezier
{
	std::vector<Vector3D> controlPoints;
	std::vector<Vector3D> curvePoints;
	int precision;

	std::vector<Vector3D> construct(std::vector<Vector3D> cP);

public:
//	int currentPointSelected;

	Bezier(void);
	~Bezier(void);

	Bezier(const std::vector<Vector3D> controlPoints, int precision = 100);

	std::vector<Vector3D> getCurvePoints() {return curvePoints; }

	std::vector<Vector3D> getControlPoints() {return controlPoints; }
	void setControlPoints(std::vector<Vector3D> cP) { controlPoints = cP; construct(); }

	Vector3D Bernstein(std::vector<Vector3D> cP, float t);

	void draw();

	// Contruct the drawed lines from control points
	void construct();
};


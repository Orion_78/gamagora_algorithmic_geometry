#pragma once



#include <math.h>
#include <stdlib.h>

//! \class Vector3D Vector3D.h
//! \brief Cette classe d�finit des vecteurs et des sommets dans l'espace.
class Vector3D
{
public:
    double x,y,z; //!< Coordinates

    Vector3D() { x=0; y=0; z=0;  } //! Empty
    Vector3D(const double& a, const double& b, const double& c) { x=a; y=b; z=c; }

    // Functions to access Vector3D class components
    double& operator[] (int i) {
        if (i == 0)    return x;
        else if (i == 1) return y;
        else	     return z;
    }

    double operator[] (int i) const {
        if (i == 0)    return x;
        else if (i == 1) return y;
        else	     return z;
    }

    // Unary operators
    Vector3D operator+ () const;
    Vector3D operator- () const;

    // Assignment operators
    Vector3D& operator+= (const Vector3D&);
    Vector3D& operator-= (const Vector3D&);
    Vector3D& operator*= (const Vector3D&);
    Vector3D& operator/= (const Vector3D&);
    Vector3D& operator*= (double);
    Vector3D& operator/= (double);

    // Binary operators
    friend Vector3D operator+ (const Vector3D&, const Vector3D&);
    friend Vector3D operator- (const Vector3D&, const Vector3D&);

    friend double operator* (const Vector3D&, const Vector3D&);

    friend Vector3D operator* (const Vector3D&, double);
    friend Vector3D operator* (double, const Vector3D&);
    friend Vector3D operator/ (const Vector3D&, double);

    friend Vector3D operator/ (const Vector3D&, const Vector3D&);

    // Boolean functions
    friend int operator==(const Vector3D&,const Vector3D&);
    friend int operator!=(const Vector3D&,const Vector3D&);
    friend int operator<(const Vector3D&,const Vector3D&);
    friend int operator>(const Vector3D&,const Vector3D&);
    //  friend Vector3D min(const Vector3D&,const Vector3D&);
    //  friend Vector3D max(const Vector3D&,const Vector3D&);

    friend Vector3D Orthogonal(const Vector3D&);

    // Norm
    double magnitude() const
	{
		return sqrt(x*x+y*y+z*z);
	}
    static Vector3D normalized(const Vector3D& u)
	{

		return u / u.magnitude();
	}

    static Vector3D Random();

	Vector3D cross(const Vector3D& v)
	{
		return Vector3D(
			y * v.z - z * v.y,
			z * v.x - x * v.z,
			x * v.y - y * v.x);
	}

    // Rotation sur un vecteur
    Vector3D Rotation( double angle);
};

// Espace 2D
inline Vector3D Vector3D::Rotation( double angle)
{
    return Vector3D(x*cos(angle)-y*sin(angle),x*sin(angle)+y*cos(angle),z);
}

//! Generates a random Vector3D with precision 1e-3 within [0,1] interval
inline Vector3D Random()
{
    double x=rand()%1001/1000.0;
    double y=rand()%1001/1000.0;
    double z=rand()%1001/1000.0;
    return Vector3D(x,y,z);
}
// Unary operators
inline Vector3D Vector3D::operator+ () const
{
    return *this;
}

inline Vector3D Vector3D::operator- () const
{
    return Vector3D(-x,-y,-z);
}

// Assignment unary operators
inline Vector3D& Vector3D::operator+= (const Vector3D& u)
{
    x+=u.x; y+=u.y; z+=u.z;
    return *this;
}

inline Vector3D& Vector3D::operator-= (const Vector3D& u)
{
    x-=u.x; y-=u.y; z-=u.z;
    return *this;
}

inline Vector3D& Vector3D::operator*= (double a)
{
    x*=a; y*=a; z*=a;
    return *this;
}

inline Vector3D& Vector3D::operator/= (double a)
{
    x/=a; y/=a; z/=a;
    return *this;
}

inline Vector3D& Vector3D::operator*= (const Vector3D& u)
{
    x*=u.x; y*=u.y; z*=u.z;
    return *this;
}

inline Vector3D& Vector3D::operator/= (const Vector3D& u)
{
    x/=u.x; y/=u.y; z/=u.z;
    return *this;
}

// Binary operators
inline Vector3D operator+ (const Vector3D& u, const Vector3D& v)
{
    return Vector3D(u.x+v.x,u.y+v.y,u.z+v.z);
}

inline Vector3D operator- (const Vector3D& u, const Vector3D& v)
{
    return Vector3D(u.x-v.x,u.y-v.y,u.z-v.z);
}

// Scalar product
inline double operator* (const Vector3D& u, const Vector3D& v)
{
    return (u.x*v.x+u.y*v.y+u.z*v.z);
}

inline Vector3D operator* (const Vector3D& u,double a)
{
    return Vector3D(u.x*a,u.y*a,u.z*a);
}

inline Vector3D operator* (double a, const Vector3D& v)
{
    return v*a;
}

// Cross product
inline Vector3D operator/ (const Vector3D& u, const Vector3D& v)
{
    return Vector3D(u.y*v.z-u.z*v.y,u.z*v.x-u.x*v.z,u.x*v.y-u.y*v.x);
}

inline Vector3D operator/ (const Vector3D& u, double a)
{
    return Vector3D(u.x/a,u.y/a,u.z/a);
}

// Boolean functions
inline int operator== (const Vector3D& u,const Vector3D& v)
{
    return ((u.x==v.x)&&(u.y==v.y)&&(u.z==v.z));
}

inline int operator!= (const Vector3D& u,const Vector3D& v)
{
    return (!(u==v));
}

/*!
\brief Compute the Euclidean norm of a Vector3D.
*/
inline double Norm(const Vector3D& u)
{
    return sqrt(u.x*u.x+u.y*u.y+u.z*u.z);
}
/*!
\brief Compute the normalized Vector3D.
*/
inline Vector3D Normalized(const Vector3D& u)
{
    return u/Norm(u);
}

inline int operator<(const Vector3D& a,const Vector3D& b)
{
    return ((a.x<b.x)&&(a.y<b.y)&&(a.z<b.z));
}

inline int operator>(const Vector3D& a,const Vector3D& b)
{
    return ((a.x>b.x)&&(a.y>b.y)&&(a.z>b.z));
}

/*!
\brief Return a new Vector3D with coordinates set to the minimum coordinates
of the two argument Vector3Ds.
*/
inline Vector3D min(const Vector3D& a,const Vector3D& b)
{
    return Vector3D(a[0]<b[0]?a[0]:b[0],a[1]<b[1]?a[1]:b[1],a[2]<b[2]?a[2]:b[2]);
}

/*!
\brief Return a new Vector3D with coordinates set to the maximum coordinates
of the two argument Vector3Ds.
*/
inline Vector3D max(const Vector3D& a,const Vector3D& b)
{
    return Vector3D(a[0]>b[0]?a[0]:b[0],a[1]>b[1]?a[1]:b[1],a[2]>b[2]?a[2]:b[2]);
}

/*!
\brief Returns a new Vector3D orthogonal to the argument Vector3D.
*/
inline Vector3D Orthogonal(const Vector3D& u)
{
    Vector3D a=Vector3D(fabs(u[0]),fabs(u[1]),fabs(u[2]));
    int i=0;
    int j=1;
    if (a[0]>a[1])
    {
        if (a[2]>a[1])
        {
            j=2;
        }
    }
    else
    {
        i=1;
        j=2;
        if (a[0]>a[2])
        {
            j=0;
        }
    }
    a=Vector3D(0.0,0.0,0.0);
    a[i]=u[j];
    a[j]=-u[i];
    return a;
}



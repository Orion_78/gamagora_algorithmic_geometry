#pragma once

#include <cmath>
#include "Vector3D.h"
#include <GL/glut.h>

class GlFactor
{
public:
	GlFactor(void);
	~GlFactor(void);

	static void drawTriangle(float size ,const float posX, const float posY, float r, float g, float b)
{
	glBegin(GL_LINES);
	glLineWidth(2.5);
	glColor3f(r, g, b);
	glVertex3f(posX - size/2, posY - size/2, 0.0);
	glVertex3f(posX + size/2, posY + size/2, 0.0);
	glEnd();

	glBegin(GL_LINES);
	glLineWidth(2.5);
	glColor3f(r, g, b);

	glVertex3f(posX + size/2, posY + size/2, 0.0);
	glVertex3f(posX + size/2, posY - size/2, 0.0);
	glEnd();

	glBegin(GL_LINES);
	glLineWidth(2.5);
	glColor3f(r, g, b);


	glVertex3f(posX + size/2, posY - size/2, 0.0);
	glVertex3f(posX - size/2, posY - size/2, 0.0);
	glEnd();

}

static void drawCircle(float rayon, float precision, float posX,float posY)
{
	float theta = (2*3.14)/precision;
	float thetaCurrent = 0;

	for (int i = 0; i<precision; i++)
	{
		glBegin(GL_LINES);
		glLineWidth(2.5);
		glColor3f(1, 1, 1);

		glVertex3f(rayon * cos( thetaCurrent ) + posX, rayon * sin ( thetaCurrent ) + posY, 0.0);
		glVertex3f(rayon * cos( thetaCurrent+theta )+ posX, rayon * sin ( thetaCurrent+theta ) + posY, 0.0);
		glEnd();

		thetaCurrent += theta;
	}

}

 static void drawCircleInside(float rayon, float precisionCercle, float iteration, float posX,float posY, float decalageX)
{
	float rayonCurrent = rayon;

	float posXCurrent = posX;
	float posYCurrent = posY;



	for (int i =0; i< iteration ; i++)
	{
		drawCircle(rayonCurrent,precisionCercle,posXCurrent,posYCurrent);

		rayonCurrent -= decalageX/iteration;
		posXCurrent -= decalageX/iteration;

		//drawCircle(0.1,100,0.4,0.5);
	}

}

static void drawCircleInline(float rayon, float precisionCercle, float iteration, float facteurDeCroissance, float posX,float posY)
{
	float rayonCurrent = rayon;
	float posXCurrent = posX;
	for (int i =0; i< iteration ; i++)
	{
		drawCircle(rayonCurrent,precisionCercle,posXCurrent,posY);
		posXCurrent += rayonCurrent;
		rayonCurrent *= facteurDeCroissance;
		posXCurrent += rayonCurrent;
	}
}

static void drawLine(Vector3D p1,Vector3D p2, Vector3D color)
{
	glBegin(GL_LINES);
	glLineWidth(2.5);
	glColor3f(color.x, color.y, color.z);

	glVertex3f(p1.x,p1.y,p1.z);
	glVertex3f(p2.x,p2.y,p2.z);
	glEnd();
}




static void drawKoch(Vector3D a, Vector3D b, int iteration)
{

	Vector3D ab = b-a;

	Vector3D i = a + ab*(1/2.0);
	
	Vector3D m = a + (ab)*(1/3.0);
	Vector3D n = b - (ab)*(1/3.0);
	Vector3D am = m - a;
	float temp = am.x;
	am.x = am.y;
	am.y = -temp;
	Vector3D o = i + am*sin(-3.14/3.0);

	if (iteration > 1)
	{
		drawKoch(a,m,iteration-1);
		drawKoch(m,o,iteration-1);
		drawKoch(o,n,iteration-1);
		drawKoch(n,b,iteration-1);
	}
	else
	{
		drawLine(a,m,Vector3D(1,1,1));
		drawLine(m,o,Vector3D(0,1,1));
		drawLine(o,n,Vector3D(1,0,1));
		drawLine(n,b,Vector3D(1,1,0));
	}
}





};


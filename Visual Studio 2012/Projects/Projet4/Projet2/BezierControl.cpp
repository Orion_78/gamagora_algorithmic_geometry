#include "BezierControl.h"


BezierControl::BezierControl()
{
	currentPointSelected = 0;
	currentBezierSelected = 0;
	//controledBezier = cb;
}


BezierControl::~BezierControl(void)
{
}


//! Quand une touche est press�e
GLvoid BezierControl::keyPressed(unsigned char key, int x, int y)
{
	switch (key)
	{
		case '1': // termine
			currentPointSelected = (currentPointSelected - 1) % ((*controledBezier)[currentBezierSelected].getControlPoints().size());
			break;
		case '4': // termine
			currentPointSelected = (currentPointSelected + 1) % ((*controledBezier)[currentBezierSelected].getControlPoints().size());
		break;

		case '3': // termine
			currentBezierSelected = (currentBezierSelected - 1) % ((*controledBezier).size());
			currentPointSelected = 0;
		break;
		case '6': // termine
			currentBezierSelected = (currentBezierSelected + 1) % ((*controledBezier).size());
			currentPointSelected = 0;
		break;
			

		case 'z': // termine
			if (currentPointSelected != -1)
			{
				std::vector<Vector3D> cp = (*controledBezier)[currentBezierSelected].getControlPoints();
				cp[currentPointSelected].y ++;
				(*controledBezier)[currentBezierSelected].setControlPoints(cp);
			}
			
		break;
		case 's': // termine
			if (currentPointSelected != -1)
			{
				std::vector<Vector3D> cp = (*controledBezier)[currentBezierSelected].getControlPoints();
				cp[currentPointSelected].y --;
				(*controledBezier)[currentBezierSelected].setControlPoints(cp);
			}
		break;
		case 'q': // termine
			if (currentPointSelected != -1)
			{
				std::vector<Vector3D> cp = (*controledBezier)[currentBezierSelected].getControlPoints();
				cp[currentPointSelected].x --;
				(*controledBezier)[currentBezierSelected].setControlPoints(cp);
			}
		break;
		case 'd': // termine
			if (currentPointSelected != -1)
			{
				std::vector<Vector3D> cp = (*controledBezier)[currentBezierSelected].getControlPoints();
				cp[currentPointSelected].x ++;
				(*controledBezier)[currentBezierSelected].setControlPoints(cp);
			}
		break;
		case 'a': // termine
			if (currentPointSelected != -1)
			{
				std::vector<Vector3D> cp = (*controledBezier)[currentBezierSelected].getControlPoints();
				cp[currentPointSelected].z --;
				(*controledBezier)[currentBezierSelected].setControlPoints(cp);
			}
		break;
		case 'e': // termine
			if (currentPointSelected != -1)
			{
				std::vector<Vector3D> cp = (*controledBezier)[currentBezierSelected].getControlPoints();
				cp[currentPointSelected].z ++;
				(*controledBezier)[currentBezierSelected].setControlPoints(cp);
			}
		break;
	}

	
}



void BezierControl::draw()
{
	

	// Draw selected point
	
		glPointSize(10);
		glBegin(GL_POINTS);
 
		  
		glVertex3f((*controledBezier)[currentBezierSelected].getControlPoints()[currentPointSelected].x,
			(*controledBezier)[currentBezierSelected].getControlPoints()[currentPointSelected].y,
			(*controledBezier)[currentBezierSelected].getControlPoints()[currentPointSelected].z);
		
		glEnd();
	
}


// CopyLeft Quentin


#ifndef _SCENE_H
#define _SCENE_H


#include "Mesh.h"
#include "opengl.h"
#include "MeshFactory.h"
#include "GlFactor.h"
#include "Bezier.h"
#include "BezierControl.h"


//! The structure Scene stores all texture Id or images used by the sceneDraw fonction
class Scene {
public:
	struct vec
	{
		float x,y,z;
	};

	std::vector<Mesh> mTabObj;

	//! Init a Scene
	void sceneInit( );

	//! Draw the scene
	void draw( );

	//! Draw the X,Y,Z axes
	void draw_axes();

	//! Draw a grid in the XZ plane
	void draw_grid();

	//! Quand une touche est press�e
	GLvoid keyPressed(unsigned char key, int x, int y);
};

#endif

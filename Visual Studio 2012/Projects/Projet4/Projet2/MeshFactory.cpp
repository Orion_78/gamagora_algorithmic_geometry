#include "MeshFactory.h"


MeshFactory::MeshFactory(void)
{
}


MeshFactory::~MeshFactory(void)
{
}

Mesh MeshFactory::sphere(float rayon, float precision)
{
	Mesh sphere;

	float decalage = 2*3.14/precision;

	// We build the hat
	///////////////////////////////
	// height
	float omega = 3.14/2 - decalage;
	// rotation
	float alpha = 0;

	Vector3D first(rayon * cos(omega)*cos(alpha),
		rayon * sin(omega) * cos(alpha),
		rayon * sin(alpha));

	sphere = circle(first.x,precision);
	sphere.translate(Vector3D( 0,first.y,0 ));
	sphere.inverseSide();
	//sphere = circle(first.x,precision).translate(Vector3D( 0,first.y,0 ));//circle(first.x,precision,Vector3D(0,first.y,0));


	// Moving the top point
	omega = 3.14/2;


	sphere.m_tabPoint[0] = Vector3D(rayon * cos(omega)*cos(alpha),
		rayon * sin(omega) * cos(alpha),
		rayon * sin(alpha));


	// Building the Body
	///////////////////////////////////


	int rowNumber = 0;

	// for all the height
	for (omega = 3.14/2 - 2 * decalage ; omega > -3.14/2  ; omega -= decalage)
	{


		Vector3D PointPosition(rayon * cos(omega)*cos(alpha),
			rayon * sin(omega) * cos(alpha),
			rayon * sin(alpha));

		Mesh circleInConstruction = circle(PointPosition.x,precision);
		circleInConstruction.translate(Vector3D( 0,PointPosition.y,0 )); //circle(PointPosition.x,precision,Vector3D(0,PointPosition.y,0));

		//Mesh circleInConstruction = cube(Vector3D(5,5,5));

		circleInConstruction.m_tabTopologie.clear();

		circleInConstruction.m_tabPoint.erase(circleInConstruction.m_tabPoint.begin());

		sphere = sphere + circleInConstruction;

		// make two triangles per side (see cylinder)
		for (int i = 1 + (precision)*rowNumber; i < (precision)*rowNumber+precision; i++)
		{
			sphere.m_tabTopologie.push_back(i);
		

			// +1 for the center
			sphere.m_tabTopologie.push_back(i+precision);
		

			sphere.m_tabTopologie.push_back(i+precision+1);
		


			sphere.m_tabTopologie.push_back(i);
			

			sphere.m_tabTopologie.push_back(i+precision+1);
			

			sphere.m_tabTopologie.push_back(i+1);
			



		}

		int i = rowNumber * precision;
		// last part
		sphere.m_tabTopologie.push_back(precision + i);
	

		sphere.m_tabTopologie.push_back(precision*2 + i);
	

		sphere.m_tabTopologie.push_back(precision+1 + i);
		


		sphere.m_tabTopologie.push_back(precision + i);
		

		sphere.m_tabTopologie.push_back(precision+1 + i);
		

		sphere.m_tabTopologie.push_back(1 + i);
	


		rowNumber++;


	}

	// Building the bottom hat
	///////////////////////////////////
	// height
	omega = -3.14/2 ;


	Vector3D last(rayon * cos(omega)*cos(alpha),
		rayon * sin(omega) * cos(alpha),
		rayon * sin(alpha));

	sphere.m_tabPoint.push_back(last);

	for (int i = 1 + (precision)*rowNumber; i < (precision)*rowNumber+precision; i++)
	{
		sphere.m_tabTopologie.push_back(i);
	

		sphere.m_tabTopologie.push_back(sphere.m_tabPoint.size()-1);
		

		sphere.m_tabTopologie.push_back(i+1);
	

	}

	sphere.m_tabTopologie.push_back((precision)*rowNumber + precision);


	sphere.m_tabTopologie.push_back(sphere.m_tabPoint.size()-1);
	

	sphere.m_tabTopologie.push_back((precision)*rowNumber + 1);
	

	sphere.inverseSide();
	sphere.computeNormal();

	return sphere;
}


Mesh MeshFactory::cylindre(Vector3D size, float precision)
{
	Mesh mUp = circle(size.x, precision);
	mUp.inverseSide();
	mUp.translate(Vector3D(0,size.y/2,0));
	Mesh mDown = circle(size.x, precision);
	mDown.translate(Vector3D(0,-size.y/2,0));
	
	Mesh m = mUp + mDown;
	
	// make two triangles per side
	for (int i = 1; i < precision; i++)
	{
		m.m_tabTopologie.push_back(i);
	

		// +1 for the center
		m.m_tabTopologie.push_back(i+precision+1);
	

		m.m_tabTopologie.push_back(i+precision+2);
	


		m.m_tabTopologie.push_back(i);
	

		m.m_tabTopologie.push_back(i+precision+2);
	

		m.m_tabTopologie.push_back(i+1);
	

	}

	// last part
	m.m_tabTopologie.push_back(precision);

	m.m_tabTopologie.push_back(precision*2+1);


	m.m_tabTopologie.push_back(precision+2);



	m.m_tabTopologie.push_back(precision);
	

	m.m_tabTopologie.push_back(precision+2);
	

	m.m_tabTopologie.push_back(1);
	

	m.inverseSide();
	m.computeNormal();
	

	return m;

}


Mesh MeshFactory::circle(float rayon, float precision)
{
	Mesh m;

	float theta = (2*3.14)/precision;
	float thetaCurrent = 0;

	// Center
	m.m_tabPoint.push_back(Vector3D());

	// Corner
	for (int i = 0; i<precision; i++)
	{
		m.m_tabPoint.push_back(Vector3D(rayon * cos( thetaCurrent ),  0, rayon * sin ( thetaCurrent )));
		//m.m_tabPoint.push_back(Vector3D(rayon * cos( thetaCurrent + theta ) + pos.x, rayon * sin ( thetaCurrent + theta ) + pos.y, pos.z));

		thetaCurrent += theta;
	}


	// Topologie
	for (int i = 1; i< precision +1; i++)
	{
		int temp = i + 1;

		if (temp > precision)
		{
			temp = 1;
		}

		

			m.m_tabTopologie.push_back(temp);

			m.m_tabTopologie.push_back(i);

			m.m_tabTopologie.push_back(0);
		
		

	}

	/*// Last triangle
	m.m_tabTopologie.push_back(0);
	

	m.m_tabTopologie.push_back(precision);


	m.m_tabTopologie.push_back(1);*/


	
	m.computeNormal();

	return m;

}

Mesh MeshFactory::cube(Vector3D size)
{
	Mesh c;

	//Center the cube
	size.x /= 2;
	size.y /= 2;
	size.z /= 2;

	c.m_tabPoint.push_back(Vector3D(-size.x,-size.y,-size.z));
	c.m_tabPoint.push_back(Vector3D(-size.x,-size.y,size.z));
	c.m_tabPoint.push_back(Vector3D(-size.x,size.y,-size.z));
	c.m_tabPoint.push_back(Vector3D(-size.x,size.y,size.z));
	c.m_tabPoint.push_back(Vector3D(size.x,-size.y,-size.z));
	c.m_tabPoint.push_back(Vector3D(size.x,-size.y,size.z));
	c.m_tabPoint.push_back(Vector3D(size.x,size.y,-size.z));
	c.m_tabPoint.push_back(Vector3D(size.x,size.y,size.z));

	c.m_tabTopologie.push_back(0);

	c.m_tabTopologie.push_back(6);

	c.m_tabTopologie.push_back(4);


	c.m_tabTopologie.push_back(0);

	c.m_tabTopologie.push_back(2);

	c.m_tabTopologie.push_back(6);


	c.m_tabTopologie.push_back(0);

	c.m_tabTopologie.push_back(3);

	c.m_tabTopologie.push_back(2);


	c.m_tabTopologie.push_back(0);

	c.m_tabTopologie.push_back(1);

	c.m_tabTopologie.push_back(3);


	c.m_tabTopologie.push_back(2);

	c.m_tabTopologie.push_back(7);

	c.m_tabTopologie.push_back(6);


	c.m_tabTopologie.push_back(2);

	c.m_tabTopologie.push_back(3);

	c.m_tabTopologie.push_back(7);


	c.m_tabTopologie.push_back(4);

	c.m_tabTopologie.push_back(6);

	c.m_tabTopologie.push_back(7);


	c.m_tabTopologie.push_back(4);

	c.m_tabTopologie.push_back(7);

	c.m_tabTopologie.push_back(5);


	c.m_tabTopologie.push_back(0);

	c.m_tabTopologie.push_back(4);

	c.m_tabTopologie.push_back(5);


	c.m_tabTopologie.push_back(0);

	c.m_tabTopologie.push_back(5);

	c.m_tabTopologie.push_back(1);


	c.m_tabTopologie.push_back(1);

	c.m_tabTopologie.push_back(5);

	c.m_tabTopologie.push_back(7);


	c.m_tabTopologie.push_back(1);

	c.m_tabTopologie.push_back(7);

	c.m_tabTopologie.push_back(3);


	c.computeNormal();

	return c;
}

/*Mesh MeshFactory::triangle(Vector3D a, Vector3D b, Vector3D c)
{
	Mesh c;

	c.m_tabPoint.push_back(a);
	c.m_tabPoint.push_back(b);
	c.m_tabPoint.push_back(c);
	c.m_tabPoint.push_back(Vector3D(-size.x,size.y,size.z));
	c.m_tabPoint.push_back(Vector3D(size.x,-size.y,-size.z));
	c.m_tabPoint.push_back(Vector3D(size.x,-size.y,size.z));
	c.m_tabPoint.push_back(Vector3D(size.x,size.y,-size.z));
	c.m_tabPoint.push_back(Vector3D(size.x,size.y,size.z));

	c.m_tabTopologie.push_back(0);

	c.m_tabTopologie.push_back(6);

	c.m_tabTopologie.push_back(4);

}*/

Mesh MeshFactory::drawCloath(Bezier a, Bezier b)
{
	Mesh c;

	
	for (int i = 1; i < a.getCurvePoints().size(); i++)
	{
		Vector3D A = a.getCurvePoints()[i];
		Vector3D C = b.getCurvePoints()[i];

		Vector3D B = a.getCurvePoints()[i-1];
		Vector3D D = b.getCurvePoints()[i-1];

		c = c + plane(A,B,C,D);
	}

	c.computeNormal();

	return c;
}


Mesh MeshFactory::plane(Vector3D A, Vector3D B, Vector3D C, Vector3D D)
{
	Mesh c;

	c.m_tabPoint.push_back( A );
	c.m_tabPoint.push_back( B );
	c.m_tabPoint.push_back( C );
	c.m_tabPoint.push_back( D );
	
	c.m_tabTopologie.push_back(0);
	c.m_tabTopologie.push_back(1);
	c.m_tabTopologie.push_back(2);

	c.m_tabTopologie.push_back(2);
	c.m_tabTopologie.push_back(1);
	c.m_tabTopologie.push_back(3);

	return c;
}




Mesh MeshFactory::drawSurfaceBalayee(const Bezier rail, const Mesh patron)
{
	Mesh result;

	Mesh copyPatron = patron;
	copyPatron.center();
	Bezier copyRail = rail;
	for (int i = 0; i<copyRail.getCurvePoints().size()-1;i++)
	{
		// Cree A et B suivant le rail dans la bonne orientation
		Mesh A = copyPatron;
		A.translate(copyRail.getCurvePoints()[i]);
		

		Mesh B = copyPatron;
		B.translate(copyRail.getCurvePoints()[i+1]);



		for (int k = 1; k < A.m_tabPoint.size(); k++)
		{
			Vector3D pointA = A.m_tabPoint[k];
			Vector3D pointC = B.m_tabPoint[k];

			Vector3D pointB = A.m_tabPoint[k-1];
			Vector3D pointD = B.m_tabPoint[k-1];

			result = result + plane(pointA,pointB,pointC,pointD);
		}

		Vector3D pointA = A.m_tabPoint[0];
		Vector3D pointC = B.m_tabPoint[0];

		Vector3D pointB = A.m_tabPoint[A.m_tabPoint.size()-1];
		Vector3D pointD = B.m_tabPoint[A.m_tabPoint.size()-1];

		result = result + plane(pointA,pointB,pointC,pointD);

	}

	result.computeNormal();

	return result;

}
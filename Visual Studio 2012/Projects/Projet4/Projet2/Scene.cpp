#include "Scene.h"

#include <cstdlib>
#include <math.h>
#include <GL/glut.h>

#define TP 6

//! view port : choissis la camera
bool b; //! Variable globale servant � ne pas r�initialiser le temps.
bool bb = false;

bool etat =true;
bool def = false;
float t2;



float t;



#if TP == 5
BezierControl bezierControl;

std::vector<Bezier> bezier;

#endif

void Scene::sceneInit( )
{
	srand(NULL);

#if TP == 2
	Mesh a=MeshFactory::cube(Vector3D(10,10,10));


	Mesh b = MeshFactory::circle(15,10);
	

	Mesh ab = a+b;


	mTabObj.push_back(ab);

	Mesh c = MeshFactory::cylindre(Vector3D(5,5,5),10);
	c.translate(Vector3D(20,10,0));

	mTabObj.push_back(c);

	Mesh s = MeshFactory::sphere(5,20);
	s.translate(Vector3D(-20,-10,0));

	mTabObj.push_back(s);

#endif


#if TP == 4
	/*Mesh writeMesh = MeshFactory::sphere(10,20);

	mTabObj.push_back(writeMesh);
	

	writeMesh.writeOFF("testwrite.off");


	Mesh readMesh;
	readMesh.readOFF("testwrite.off");
	mTabObj.push_back(readMesh);

	readMesh.writeOFF("testreadwrite.off");

	
	Mesh writeMesh = MeshFactory::sphere(10,20);
	writeMesh.translate(Vector3D(10,0,5));
	//writeMesh.center();
	//writeMesh.scale(2);
	writeMesh.normalize();

	mTabObj.push_back(writeMesh);*/

	Mesh readMesh;
	readMesh.readOFF("triceratops.off");
	readMesh.normalize();
	readMesh.scale(20);
	mTabObj.push_back(readMesh);
	/*


	readMesh.writeOFF("test.off");
	readMesh.readOFF("test.off");*/

#endif

#if TP == 5
	std::vector<Vector3D> controlPoints;
	controlPoints.push_back( Vector3D( -20.f,-20.f,0.f ) );
	controlPoints.push_back( Vector3D( -10.f,10.f,0.f ) );
	controlPoints.push_back( Vector3D( 10.f,10.f,0.f ) );
	controlPoints.push_back( Vector3D( 20.f,-20.f,0.f ) );
	controlPoints.push_back( Vector3D( 30.f,-20.f,0.f ) );
	controlPoints.push_back( Vector3D( 30.f,-10.f,0.f ) );

	bezier.push_back(Bezier(controlPoints,10));
	controlPoints.clear();

	controlPoints.push_back( Vector3D( -20.f,-20.f,20.f ) );
	controlPoints.push_back( Vector3D( -10.f,10.f,20.f ) );
	controlPoints.push_back( Vector3D( 10.f,10.f,20.f ) );
	controlPoints.push_back( Vector3D( 20.f,-20.f,20.f ) );
	controlPoints.push_back( Vector3D( 30.f,-20.f,20.f ) );
	controlPoints.push_back( Vector3D( 30.f,-10.f,20.f ) );


	
	

	bezier.push_back(Bezier(controlPoints,10));


	bezierControl.controledBezier = &bezier;

//	mTabObj.push_back( MeshFactory::drawCloath(bezier[0], bezier[1] ) );
	Mesh c = MeshFactory::circle(10,10);
	c.m_tabPoint.erase(c.m_tabPoint.begin());
	mTabObj.push_back( MeshFactory::drawSurfaceBalayee(bezier[0],  c) );



#endif

}




void Scene::draw_axes()
{
	glLineWidth(5);
	glBegin( GL_LINES );

	glColor3f( 1.f, 0.f, 0.f);
	glVertex3f( 0.f, 0.f, 0.f);
	glVertex3f( 1.f, 0.f, 0.f);

	glColor3f( 0.f, 1.f, 0.f);
	glVertex3f( 0.f, 0.f, 0.f);
	glVertex3f( 0.f, 1.f, 0.f);

	glColor3f( 0.f, 0.f, 1.f);
	glVertex3f( 0.f, 0.f, 0.f);
	glVertex3f( 0.f, 0.f, 1.f);

	glEnd();
}

void Scene::draw_grid()
{
	int i;
	glLineWidth(1);
	glColor3f( 1.f, 1.f, 1.f);

	glPushMatrix();
	glTranslatef( -5, 0, -5);

	glBegin( GL_LINES );

	for (i=0;i<=10;++i)
	{
		glVertex3f( i, 0, 0);
		glVertex3f( i, 0, 10);
	}

	for (i=0;i<=10;++i)
	{
		glVertex3f( 0, 0, i);
		glVertex3f( 10, 0, i);
	}

	glEnd();
	glPopMatrix();
}

// TP 3 ////////////////////////////////////////////////
std::vector< double > decalageBoule;
bool init = true;
double rot = 0;
double speed = 1;
void teeClock(int numberOfTee, double rot, double initSize, double coefSize, double speedSphere)
{
	if (init)
	{
		for (int i = 0; i< numberOfTee; i++)
		{
			decalageBoule.push_back(0);
		}

		init = false;
	}

	glRotated(rot,0,0,1);



	for (int i = 0; i< numberOfTee; i++)
	{
		glTranslated(0, initSize*2 +  initSize * (coefSize * i ),0);

		glutWireTeapot(initSize + initSize * coefSize * i );

		glPushMatrix();


		glRotated(rot + decalageBoule[i],0,1,0);
		decalageBoule[i] += speedSphere*i;
		glTranslated(initSize + initSize * (coefSize * i),0,0);

		glutWireSphere(3,20,20);
		glPopMatrix();



	}
}
// TP 3 Fin ////////////////////////////////////////////////

void Scene::draw( void )
{
	glDisable(GL_TEXTURE_2D); // vidage des texture au pr�alable pour �viter les bug

	for ( Mesh m : mTabObj){
		m.draw();
	}

#if TP == 3
	rot += speed;
	teeClock(4,rot,10,-0.2,speed);
#endif

#if TP == 5
	for (Bezier b : bezier)
		b.draw();
	bezierControl.draw();
#endif
}


//! Quand une touche est press�e
GLvoid Scene::keyPressed(unsigned char key, int x, int y)
{
#if TP == 5
	bezierControl.keyPressed(key,x,y);
#endif
}
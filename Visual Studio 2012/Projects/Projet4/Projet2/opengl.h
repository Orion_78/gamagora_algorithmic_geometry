// Informatique graphique LIF041 (Alexandre.Meyer -@- liris.cnrs.fr

#ifndef _OPENGL_HLIF041
#define _OPENGL_HLIF041

#include <stdlib.h>
#include <stdio.h>

#include "Scene.h"
#include "Camera.h"
#include <GL/glut.h>
#include "Vector3D.h"


//! A appeller juste avant la boucle principale (Main Loop)
void GLInit(GLsizei Width, GLsizei Height);

//! Quand la fenetre est 'resiz�e'
void GLResize(GLsizei Width, GLsizei Height);

//! Pour changer la couleur du Ambiant/Diffus/Speculaire en m�me temps
void GLColor(const float r, const float g, const float b);

//! The main drawing function
void GLDraw(void);

//! Quand une touche est press�e
void keyPressed(unsigned char key, int x, int y);

//! The function called whenever a normal key is pressed
void specialKeyPressed(int key, int x, int y);

//! Load texture and return the GL identifier
unsigned int LoadGLTexture(const char* nomfichier, bool isTransparency);

#endif

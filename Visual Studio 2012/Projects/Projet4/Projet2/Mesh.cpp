#include "Mesh.h"


Mesh::Mesh(void)
{
}


Mesh::~Mesh(void)
{
}

Mesh::Mesh(const Mesh & m): m_tabPoint(m.m_tabPoint), m_tabTopologie(m.m_tabTopologie), m_tabNorm(m.m_tabNorm)
{}

Mesh operator + (const Mesh& scene, const Mesh& ajout)
{
    Mesh ret(scene);

    for (unsigned int i = 0 ; i < ajout.m_tabPoint.size(); i ++)
    {
        ret.m_tabPoint.push_back(ajout.m_tabPoint[i]);
    }
    
    for (unsigned int i = 0 ; i < ajout.m_tabTopologie.size(); i++)
    {
		ret.m_tabTopologie.push_back(ajout.m_tabTopologie[i] + scene.m_tabPoint.size());		
    }

	for (Vector3D n : ajout.m_tabNorm)
    {
		ret.m_tabNorm.push_back(n);
    }


    return ret;
}


void Mesh::draw()
{
	for (unsigned int i = 0; i<m_tabTopologie.size(); i+=3)
	{
		// Nouvelle methode
		//glNormalPointer(GL_DOUBLE, 0, tabNormales);
		//glDrawElements(GL_TRIANGLES,nbTriangle*3,GL_UNSIGNED_INT,tabTriangle);
			

		// ancienne methode
		glColor3f(0.7,0.7,0.7);
		
		glBegin(GL_TRIANGLES);
			glNormal3f(m_tabNorm[m_tabTopologie[i]].x,m_tabNorm[m_tabTopologie[i]].y,m_tabNorm[m_tabTopologie[i]].z);
			glVertex3f(m_tabPoint[m_tabTopologie[i]].x,m_tabPoint[m_tabTopologie[i]].y,m_tabPoint[m_tabTopologie[i]].z);
			glNormal3f(m_tabNorm[m_tabTopologie[i+1]].x,m_tabNorm[m_tabTopologie[i+1]].y,m_tabNorm[m_tabTopologie[i+1]].z);
			glVertex3f(m_tabPoint[m_tabTopologie[i+1]].x,m_tabPoint[m_tabTopologie[i+1]].y,m_tabPoint[m_tabTopologie[i+1]].z);
			glNormal3f(m_tabNorm[m_tabTopologie[i+2]].x,m_tabNorm[m_tabTopologie[i+2]].y,m_tabNorm[m_tabTopologie[i+2]].z);
			glVertex3f(m_tabPoint[m_tabTopologie[i+2]].x,m_tabPoint[m_tabTopologie[i+2]].y,m_tabPoint[m_tabTopologie[i+2]].z);
		glEnd();
	}



	/*Vector3D p1 = Vector3D(-1.0,1.0,0.0);
			Vector3D p2 = Vector3D(1,1,0);
			Vector3D p3 = Vector3D(1,2,0);
			//tableau de points
			Vector3D tab[2];

			tab[0]=p1;
			tab[1]=p2;

			glColor3f(0.7,0.7,0.7);
			glBegin(GL_TRIANGLES);	
				glVertex3f(tab[0].x,tab[0].y, tab[0].z);
				glVertex3f(tab[1].x,tab[1].y, tab[1].z);
				glVertex3f(p3.x,p3.y,p3.z);
	glEnd();*/

		

}

Vector3D Mesh::getSize()
{
	Vector3D sizeMax;
	Vector3D sizeMin;
	for (Vector3D v : m_tabPoint)
	{
		if (v.x > sizeMax.x)
		{
			sizeMax.x = v.x;
		}
		if (v.x < sizeMin.x)
		{
			sizeMin.x = v.x;
		}

		if (v.y > sizeMax.y)
		{
			sizeMax.y = v.y;
		}
		if (v.y < sizeMin.y)
		{
			sizeMin.y = v.y;
		}

		if (v.z > sizeMax.z)
		{
			sizeMax.z = v.z;
		}
		if (v.z < sizeMin.z)
		{
			sizeMin.z = v.z;
		}
	}

	return Vector3D(sizeMax.x-sizeMin.x, sizeMax.y-sizeMin.y, sizeMax.z-sizeMin.z);

}

void Mesh::scale(float scale)
{
	for (Vector3D& v : m_tabPoint)
	{
		v *= scale;
	}
}

void Mesh::center()
{
	// find barycentre
	Vector3D bar;
	for (Vector3D v : m_tabPoint)
	{
		bar += v;
	}

	bar /= m_tabPoint.size();

	translate(-bar);
}

void Mesh::normalize()
{
	center();

	Vector3D size = getSize();

	scale(1/(size.magnitude()/2));
}


void Mesh::writeOFF(std::string path)
{
	std::ofstream myfile;
	myfile.open (path);
	  myfile << "OFF\n";


	  myfile << m_tabPoint.size() << " " << m_tabTopologie.size()/3 << " " << "0\n";

	  for (Vector3D v : m_tabPoint)
	  {
		  myfile << v.x << " " << v.y << " " << v.z << "\n";
	  }
	  
	  for (unsigned int i = 0; i< m_tabTopologie.size(); i += 3)
	  {
		  myfile << "3 " << m_tabTopologie[i] << " " << m_tabTopologie[i+1] << " " << m_tabTopologie[i+2] << "\n";
	  }

	  myfile.close();
}

void Mesh::readOFF(std::string path)
{
	std::ifstream myfile;
	myfile.open (path);

	std::string line; // variable contenant chaque ligne lue 
	std::vector<std::string> elems;
		
	std::getline( myfile, line );
	std::getline( myfile, line );

	// const std::string &s, char delim, std::vector<std::string> &elems)
	
	Tools::split(line, ' ',elems); 

	int numberOfPoint = std::atoi( elems[0].c_str() );
	int numberOfTypology = std::atoi( elems[1].c_str() );

	for (int i = 0; i < numberOfPoint ; i++)
	{
		std::getline( myfile, line );
		Tools::split(line, ' ',elems); 

		m_tabPoint.push_back(Vector3D ( std::atof( elems[0].c_str()), std::atof( elems[1].c_str()), std::atof( elems[2].c_str())));

	}
      
	for (int i = 0; i < numberOfTypology ; i++)
	{
		std::getline( myfile, line );
		Tools::split(line, ' ',elems);

		m_tabTopologie.push_back(std::atoi( elems[1].c_str() ));
		m_tabTopologie.push_back(std::atoi( elems[2].c_str() ));
		m_tabTopologie.push_back(std::atoi( elems[3].c_str() ));
	

	}

	computeNormal();

	myfile.close();
}


void Mesh::translate(Vector3D vector)
{
	for (Vector3D& v : m_tabPoint)
	{
		v = v + vector;
	}
}


void Mesh::rotation(int composante, double angleDegree)
{
    angleDegree = M_PI * angleDegree / 180;

    double sinAngle = sin ( angleDegree );
    double cosAngle = cos ( angleDegree );
    switch (composante) {
    // Rotation autour de X
    case 0:
        for(int i = 0; i < m_tabPoint.size(); i ++)
        {
			Vector3D d = m_tabPoint[i];
            m_tabPoint[i] = Vector3D(d.x,
                                    cosAngle * d.y - sinAngle * d.z,
                                    sinAngle * d.y + cosAngle * d.z);
        }

        for(int i = 0; i < m_tabNorm.size(); i ++)
        {
            Vector3D v = m_tabNorm[i];
            m_tabNorm[i] = Vector3D(
                        v[0],
                    cosAngle * v[1] - sinAngle * v[2],
                    sinAngle * v[1] - cosAngle * v[2]);
        }

        break;
    // Autour de Y
    case 1:
        for(int i = 0; i < m_tabPoint.size(); i ++)
        {
            Vector3D d = m_tabPoint[i];
            m_tabPoint[i] = Vector3D(cosAngle * d.x + sinAngle * d.z,
                                    d.y,
                                    - sinAngle * d.x + cosAngle * d.z);
        }

        for(int i = 0; i < m_tabNorm.size(); i ++)
        {
            Vector3D v = m_tabNorm[i];
            m_tabNorm[i] = Vector3D(
                        cosAngle * v[0] + sinAngle * v[2],
                    v[1],
                    -sinAngle * v[0] - cosAngle * v[2]);
        }

        break;
        //Z
    default:
        for(int i = 0; i < m_tabPoint.size(); i ++)
        {
            Vector3D d = m_tabPoint[i];
            m_tabPoint[i] = Vector3D(cosAngle * d.x- sinAngle * d.y,
                                    sinAngle * d.x+ cosAngle * d.y,
                                    d.z);
        }

        for(int i = 0; i < m_tabNorm.size(); i ++)
        {
            Vector3D v = m_tabNorm[i];
            m_tabNorm[i] = Vector3D(
                        cosAngle * v[0] - sinAngle * v[1],
                    sinAngle * v[0] - cosAngle * v[1],
                    v[2]);
        }
        break;
    }

	computeNormal();
}

void Mesh::rotation(double thetaX, double thetaY, double thetaZ)
{
    rotation(0, thetaX);
    rotation(1, thetaY);
    rotation(2, thetaZ);
}

void Mesh::inverseSide()
{
	for (unsigned int i = 0; i<m_tabTopologie.size(); i+=3)
	{
		unsigned int temp = m_tabTopologie[i];
		m_tabTopologie[i] = m_tabTopologie[i+2];
		m_tabTopologie[i+2] = temp;
	
	}
}

void Mesh::computeNormal()
{
	// init
	m_tabNorm.clear();
	for (Vector3D v : m_tabPoint)
	{
		m_tabNorm.push_back(Vector3D());
	}

	for (unsigned int i = 0; i<m_tabTopologie.size(); i+=3)
	{
		Vector3D ab = m_tabPoint[m_tabTopologie[i+1]] - m_tabPoint[m_tabTopologie[i]];
		Vector3D ac = m_tabPoint[m_tabTopologie[i+2]] - m_tabPoint[m_tabTopologie[i]];
		// ab.cross(ac)
		m_tabNorm[m_tabTopologie[i]] += Vector3D::normalized( ab.cross(ac) );
		m_tabNorm[m_tabTopologie[i+1]] += Vector3D::normalized( ab.cross(ac) );
		m_tabNorm[m_tabTopologie[i+2]] += Vector3D::normalized( ab.cross(ac) );
	
	}

	for (Vector3D v : m_tabNorm)
	{
		v = Vector3D::normalized(v);
	}

}

// Simplification par angle en cours
/*void Mesh::simplificationMaillage(float ecartAngleMin)
{
	for (int i = 0 ; i < m_tabPoint.size(); i++)
	{
		Vector3D ba = m_tabPoint[i] - m_tabPoint[i+1];
		Vector3D ca = m_tabPoint[i] - m_tabPoint[i+2];
		Vector3D bc = m_tabPoint[i+2] - m_tabPoint[i+1];

		float a = bc.magnitude ;
		float b = ca.magnitude ;
		float c = ba.magnitude ;

		float angle = acos((b*b + c*c + a*a) / 2*b*c);

		

		if (angle < ecartAngleMin || abs(180-angle) < ecartAngleMin || (360 - angle) < ecartAngleMin)
		{
			m_tabPoint.erase(
		}
	}
}*/


// QEM plan plat grace au normal ?


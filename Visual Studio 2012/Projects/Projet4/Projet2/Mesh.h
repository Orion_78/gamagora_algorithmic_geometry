#pragma once


#include "Vector3D.h"
#include <vector>
#include <GL/glut.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include "Tools.h"
#include "Matrice4x4.h"


class Mesh 
{
public:
	std::vector<Vector3D> m_tabPoint;
	std::vector<unsigned int> m_tabTopologie;
	std::vector<Vector3D> m_tabNorm;

	Mesh(void);
	~Mesh(void);
	Mesh(const Mesh&);

	// draw the model inside the screen
	virtual void draw();

	friend Mesh operator + (const Mesh&, const Mesh&);

	void writeOFF(std::string path);

	void readOFF(std::string path);

	void translate(Vector3D vector);

	void rotation(int composante, double angleDegree);

	void rotation(double thetaX, double thetaY, double thetaZ);

	void scale(float scale);

	void inverseSide();

	void normalize();

	Vector3D getSize();

	void center();

	void computeNormal();

//	void simplificationMaillage(float ecartAngleMin);
};


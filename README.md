2014 - Gamagora, Lyon

Geometric Modeling done in 2014 at Gamagora Schools with [Gilles Gesquiere](http://liris.cnrs.fr/membres/?id=4206).

7 realisations, more details (in french) available in the Realisation 3D folder within the source code :

TP 1:
Drawing using Open GL and draw lines functions

Snapshot:

```
#!c++

glBegin(GL_LINES);
	glLineWidth(2.5);
	glColor3f(1, 1, 1);

	glVertex3f(rayon * cos( thetaCurrent ) + posX, rayon * sin ( thetaCurrent ) + posY, 0.0);
	glVertex3f(rayon * cos( thetaCurrent+theta )+ posX, rayon * sin ( thetaCurrent+theta ) + posY, 0.0);
glEnd();
```


![koch.png](https://bitbucket.org/repo/XzdEBb/images/2259155838-koch.png)

TP 2:

Building a structure and display 3D models from vertices, topology and normals.
Build simple object as a cube a sphere a plane and a cylinder.


```
#!c++

class Mesh 
{
public:
	std::vector<Vector3D> m_tabPoint;
	std::vector<unsigned int> m_tabTopologie;
	std::vector<Vector3D> m_tabNorm;
```


![basicGeometry.png](https://bitbucket.org/repo/XzdEBb/images/2444576190-basicGeometry.png)


TP 3:

Use power of hierarcy to make them animate in realtime.

![teepot.png](https://bitbucket.org/repo/XzdEBb/images/2549929825-teepot.png)

TP 4:

Load and draw .obj models using good normals.

![dyno.png](https://bitbucket.org/repo/XzdEBb/images/1937160400-dyno.png)


TP 5 to 7:

Draw and control in real time a 3D bezier curve.
Use it to make a parametric surface.

![bezier curve and parametric surface.png](https://bitbucket.org/repo/XzdEBb/images/725780730-bezier%20curve%20and%20parametric%20surface.png)

Extra :
Courbe Hermit

![Hermit.png](https://bitbucket.org/repo/XzdEBb/images/378790681-Hermit.png)